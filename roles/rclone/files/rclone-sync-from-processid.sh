#!/bin/bash

readonly TM_PROCESS_API='https://front.tm.bsf-intranet.org/goto/temps-modernes/api/process'
readonly CURL='curl --silent --insecure'
readonly FILESLIST_DIR="${FILESLIST_DIR:-.}"
readonly BUCKET_DIR='hugo'



logthis() {
    ts="$( date '+%Y/%m/%d %H:%M:%S' )"
    >&2 echo "${ts} $( basename "$0") $*"
}
processid_to_fileslist(){
    local process_id=$1
    json=$( $CURL "${TM_PROCESS_API}/?id=${process_id}" )
    [ "$json" == "{}" ] && {
        logthis "ERROR: invalid process_id: $process_id"
        return
    }
    echo "$json" \
        | jq -r '
            .deployment.cap_config.containers[] 
            | select((.name=="mediacenter") or (.name=="ideascube")) 
            | .content[].name
        ' \
        | sort \
        | sed -e "s@^@${BUCKET_DIR}/@" \
            -e 's@$@.zip@' \
    > "${FILESLIST_DIR}/${process_id}.list"
}
run_sync(){
    logthis "Start Sync files from process_id files"
    # feed rclone with merged files via stdin
    find "$FILESLIST_DIR" -type f -name '*.list' -exec cat {} + \
        | sort -u \
        | rclone copy \
            -v wasabi:olip-packages-prod \
            --files-from - \
            /data/olip-packages-prod
    logthis "End Sync files from process_id files"
}



[ -d "$FILESLIST_DIR" ] || {
    logthis "Create directory for fileslist files..."
    mkdir -p "$FILESLIST_DIR" || exit 2
}

for process_id in "$@" ; do
    [ -r "${FILESLIST_DIR}/${process_id}.list" ] && continue
    logthis "Build files list for $process_id"
    processid_to_fileslist "$process_id"
done

run_sync
