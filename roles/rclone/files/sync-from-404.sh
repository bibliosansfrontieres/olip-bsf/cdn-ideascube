#!/bin/bash


# from the Cube
# from packages to be installed, get urls, format as filesfrom list
# \sqlite3 /data/app.db "select download_path from installed_contents where target_state = 'installed' and current_state = 'uninstalled' " | sed -e 's@#unzip@@;s@http://s3.eu-central-1.wasabisys.com/@@'


FILESLIST_FILE=/tmp/missing.list
readonly BUCKET_NAME='olip-packages-prod'

not_found=$( awk ' $7~/'"$BUCKET_NAME"'/ && $9=/404/ { print $7}' /var/log/nginx/access.log | sort -u )

rm -f "$FILESLIST_FILE"

for i in $not_found ; do
    [ -f "/data/$i" ] && continue
    echo >&2 "File not found: $i"
    echo "$i" >> "$FILESLIST_FILE"
done

[ ! -f "$FILESLIST_FILE" ] && {
    echo >&2 "No missing files found."
    exit 0
}


sed -i -e 's@^/@@' "$FILESLIST_FILE"
chmod a+r "$FILESLIST_FILE"

echo >&2 "list is $(wc -l "$FILESLIST_FILE" | awk '{ print $1}' ) files long"

sudo -u cdn rclone copy -v wasabi: --files-from "$FILESLIST_FILE"  /data/
sudo chmod a+rX -R "/data/$BUCKET_NAME"
