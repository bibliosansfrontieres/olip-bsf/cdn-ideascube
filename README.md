# BSF local CDN server

The (so-called) CDN is a cache server that synchronize remote content locally
to make it available to devices such as Ideascube server.

This is the technical documentation: hardware build, OS setup, CDN install.

For our end-users, the documentation is still in [the (internal) wiki](https://wiki.bsf-intranet.org/doku.php?id=outils_internes:temps-modernes:user:cdn).

## Services

* `avahi` broadcasts several FQDNs (i.e. `cdn-ideascube.local`)
* `samba` shares the Kolibri channels
* `rclone` takes care of the contents synchronisation
* `nginx` allows to brose the contents - and serves a few stats

## Installation

These instructions are for an ARM based board, such as the
[Olimex LIME2](https://www.olimex.com/Products/OLinuXino/A20/A20-OLinuXino-LIME2/).

This playbook has been successfully tested on the following devices:

* `arm`: Olimex Lime2
* `amd64`: Peren-IT server, ProLiant ML350p Gen8

Basically, any computer with a big HDD and a network interface will do.
You will probably have to adapt the instructions here and there.

### HDD setup

You need a big HDD - as of writing, a ~4Tb drive is required.

The HDD requires a single partition to be created.
Since `fdisk(1)` will get limited to 2Tb, you have to use `gdisk(1)`
(for our current need, the commands are exactly identical).

The partition must then be formated to `ext4`.
The Fine One Liner is : `mkfs.ext4 -m 0 -L contents /dev/sda1`

* `-m 0`: ensures we don't loose the (default) 5% of the filesystem
  blocks usually reserved for the super-user.
* `-L data`: because you don't want to mix the drives/volumes up :-)
* `/dev/sda1`: please use the correct partition.

Note that you can apply these fine tunings afterwards:
`tune2fs -m 0 -L contents /dev/sda1`

### OS setup

For ARM/Olimex:

* Download the [Armbian Debian Buster image](https://armbian.hosthatch.com/archive/lime2/archive/Armbian_21.08.1_Lime2_buster_current_5.10.60.img.xz)
* Burn an SD card and insert it in an OLIMEX Lime 2
* Grab a very good 5V / 2A power supply
* Power the board

You can find help on the
[Armbian website](https://docs.armbian.com/User-Guide_Getting-Started/).

SSH access:

* login : `root`
* password : `1234`

For other hardware: proceed to a fresh Debian 10 Buster installation
(Debian 11 Bullseye is not supported - yet).

### CDN install

#### Initial run

The first time you want to apply the playbook,
the easy way is to use this one-liner:

```shell
curl -sfL https://gitlab.com/bibliosansfrontieres/olip-bsf/cdn-ideascube/-/raw/master/go.sh | bash -s -- --country france --project_name bib
```

This will install all the required dependencies,
then it will run the playbook with some default options.

#### Future runs

The playbook can be used with custom variables,
allowing further configuration customization:

```shell
ansible-playbook -l localhost -u root main.yml --extra-vars "country=france project_name=bib"
```

#### The log file

Running the playbook may get you this error message:

```text
[WARNING]: log file at /var/log/ansible-pull.log is not writeable and we cannot create it, aborting
```

Create the file so it is writable by the user running the `ansible-playbook` command.
For example:

```shell
sudo touch /var/log/ansible-pull.log
sudo chgrp adm /var/log/ansible-pull.log
sudo chmod g+w /var/log/ansible-pull.log
```

#### Check mode

If using the `--check` option, you need to install `python-apt` first.

## Configuration variables

Some variables you can use as `--extra-vars`:

| variable | description | default value | example custom value |
| -------- | ----------- |  ------------ | -------------------- |
| `country` | Country Code, ISO format | n/a | `fra` |
| `project_name` | Local identifier for the device | n/a | `montreuil` |
| `external_hdd` | Set this to `true` if you use a custom `storage_device`. | `false` | `true` |
| `storage_device` | Content storage partition | `/dev/sda1` | `/dev/sdb1` |
| `storage_mountpoint` | Mountpoint for the Contents partition | `/data` | `/media/hdd` |
| `server_interface_name` | Network interface name | `eth0` | `ens18` |
| `wasabi_access_key_id` | Wasabi credentials part 1 | n/a | n/a |
| `wasabi_secret_access_key` | Wasabi credentials part 2 | n/a | n/a |

## Wrapper script

A script to run the playbook using most of these extra variables
could look like this:

```shell
ansible-playbook -l localhost -u root main.yml \
    --extra-vars "country=bsf project_name=testcdn" \
    --extra-vars "wasabi_access_key_id=***** wasabi_secret_access_key=*****" \
    --extra-vars "external_hdd=true storage_device=/dev/sdb1" \
    --extra-vars "server_interface_name=ens18" \
    $@
```

And would be used such as: `./playbook.sh --tags logs --check`

This is a convenient way to ensure the next playbook runs will use the same values.

## Provided tooling

### speedtest

A basic script runs `speedtest` at regular intervals, the results are stored in CSV.

It is planned to replace this by a proper tool (see [#18](https://gitlab.com/bibliosansfrontieres/olip/devices/cdn-ideascube/-/issues/18))

### Devices scanning

In order to find the Ideaascube devices connected to your LAN,
the `scan-for-devices` will search for devices exposing SSH port (22):

```text
tom@cdn-bgd-coxsbazar ~ $ scan-for-devices
Looking for devices in the 192.168.1.0/24 subnet...
192.168.1.103
192.168.1.127
192.168.1.158
192.168.1.185
```

### Contents web view

The CDN device exposes some of its contents and tooling in a web interface.

It is served on all interfaces,
so you can use the plain IP address or the hostname as URL.

Here's a text-based browser dump as an example:

```text
$ elinks -dump http://192.168.88.250
                            Welcome to cdn-bdi-buja

   This webserver is a local CDN for Ideascube devices.

     • [1]Browse the CDN files
     • [2]Check the Synchronisation logs

   If on Windows OS or Android the link above might not work.
   Add this to your OS host file :

     192.168.88.250 cdn-bdi-buja.local


   ══════════════════════════════════════════════════════════════════════════

Network statistics for eno1

   Summary  Hourly  Daily  Monthly  Top 10

References

   Visible links
   1. http://cdn-bdi-buja.local/
   2. http://localhost/rclone-wasabi.log

```
